//
//  ViewController.swift
//  UsarCamara&Imagenes
//
//  Created by Jose Tinajero on 28/7/17.
//  Copyright © 2017 Jose Tinajero. All rights reserved.
//

import UIKit
import MobileCoreServices //para hacer uso de camara e Imagenes
import AVFoundation //para hacer uso de videos

class ViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    @IBOutlet weak var picturesImageView: UIImageView!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func camaraButtonPressed(_ sender: Any) {
        
        
        
        
    }
    
    
    @IBAction func camaraRollButtonAction(_ sender: Any) {
        
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary){
            
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = .photoLibrary //imagePicker.sourceType = .camera usamos este valor para hacer uso de la camara del telefono
            imagePicker.allowsEditing = false
            imagePicker.mediaTypes = [kUTTypeImage as String, kUTTypeMovie as String]
            
            present(imagePicker,animated: true, completion: nil)
            
            //nuevaImagen = false Creamos una nueva imagen en donde se almacena la foto obtenida con el telefono
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        self .dismiss(animated: true, completion: nil)
        
        let mediaType = info[UIImagePickerControllerMediaType] as! NSString
        
        if mediaType.isEqual(to: kUTTypeImage as String){
            
            let image = info[UIImagePickerControllerOriginalImage] as! UIImage
            
            picturesImageView.image = image
            
            //if nuevaImagen{
                
            //    UIImageWriteToSavedPhotosAlbum(image, self, nil, nil)
                
            //} Esn esta linea podemos hacer que se guardesn en la libreria las fotos que tomamos con la camara del telefono
            
            
        }
        
    }

}

